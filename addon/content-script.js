function UrlWithUofTAccessMaker(domain, url) {
    return url.replace(domain, domain.replaceAll(".", "-") + ".myaccess.library.utoronto.ca")
}

chrome.runtime.onMessage.addListener(
    function (request) {
        if (request.message === "clicked_browser_action") {
            const domain = window.location.hostname
            const url = window.location.href
            chrome.runtime.sendMessage({"message": "redirect_to_uoft_access", "url": UrlWithUofTAccessMaker(domain, url)})
        }
    }
)
