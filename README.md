# UofT myaccess #

Original author: Pan Chen
Revisions by: Andrew Petersen (andrew.petersen@utoronto.ca)

This add-on, which is designed for firefox, translates the active tab's URL to use the University of Toronto's
myaccess service in order to get access to paywalled documents.
